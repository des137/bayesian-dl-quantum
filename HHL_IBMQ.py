import matplotlib.pyplot as plt
import seaborn as sns
import os
from ast import literal_eval
from math import pi
from qiskit import QuantumCircuit, ClassicalRegister, QuantumRegister


def get_hhl_2x2():
    '''Generate a circuit that implements the full HHL algorithm for the case
    of A = 0.5*[[3, 1], [1, 3]] and b = [1, 0].

    :param noise: (bool) True if a noisy simulation is desired.

    :return: A QISKit program to perform HHL.
    '''
    q = QuantumRegister(6)
    c = ClassicalRegister(2)
    hhl = QuantumCircuit(q, c)
    # Phase estimation subroutine
    hhl.h(q[1])
    hhl.h(q[2])
    # Controlled-U0
    hhl.cu3(-pi / 2, -pi / 2, pi / 2, q[2], q[3])
    hhl.cu1(3 * pi / 4, q[2], q[3])
    hhl.cx(q[2], q[3])
    hhl.cu1(3 * pi / 4, q[2], q[3])
    hhl.cx(q[2], q[3])
    # Controlled-U1
    hhl.cx(q[1], q[3])
    # Inverse Fourier transform
    hhl.swap(q[1], q[2])
    hhl.h(q[2])
    hhl.cu1(-pi / 2, q[1], q[2])  # Dagger(Controlled-S)
    hhl.h(q[1])
    # Eigenvalue inversion subroutine
    hhl.swap(q[1], q[2])
    # Controlled rotation subroutine
    hhl.cu3(0.392699, 0, 0, q[1], q[0])  # Controlled-RY0
    hhl.cu3(0.19634955, 0, 0, q[2], q[0])  # Controlled-RY1
    # Uncomputations
    hhl.swap(q[1], q[2])
    hhl.h(q[1])
    hhl.cu1(pi / 2, q[1], q[2])  # Inverse(Dagger(Controlled-S))
    hhl.h(q[2])
    hhl.swap(q[2], q[1])
    # Inverse(Controlled-U1)
    hhl.cx(q[1], q[3])
    # Inverse(Controlled-U0)
    hhl.cx(q[2], q[3])
    hhl.cu1(-3 * pi / 4, q[2], q[3])
    hhl.cx(q[2], q[3])
    hhl.cu1(-3 * pi / 4, q[2], q[3])
    hhl.cu3(-pi / 2, pi / 2, -pi / 2, q[2], q[3])
    # End of Inverse(Controlled-U0)
    hhl.h(q[2])
    hhl.h(q[1])

    # SWAP test subroutine
    # Target state preparation
    hhl.rz(-pi, q[4])
    hhl.u1(pi, q[4])
    hhl.h(q[4])
    hhl.ry(-0.9311623288419387, q[4])
    hhl.rz(pi, q[4])
    # Swap test
    hhl.h(q[5])
    hhl.cx(q[4], q[3])
    hhl.ccx(q[5], q[3], q[4])
    hhl.cx(q[4], q[3])
    hhl.h(q[5])

    hhl.barrier(q)
    hhl.measure(q[0], c[0])
    hhl.measure(q[5], c[1])
    return hhl


def create_plot():
    '''Create the plot for the success probability of the HHL protocol for
    simulations and runs in the QPU.

    :return: (matplotlib.pyplot) The plot.
    '''
    all_files = os.listdir('output/')
    files = [file for file in all_files if file.split('_')[0] == 'IBMQX']
    files = sorted(files, key=str)
    models = []
    modelnames = []
    hardware = []
    hardwarenames = []
    first_noiseless = True
    for file in files:
        name_parts = file.split('_')
        if name_parts[1] == 'QPU':
            hardware.append(get_psuccess('output/' + file))
            hardwarenames.append(name_parts[0])
        else:
            if name_parts[2] != '0.0':
                models.append(get_psuccess('output/' + file))
                modelnames.append(name_parts[1] + '=' + name_parts[2])
            else:
                if first_noiseless:
                    models = [get_psuccess('output/' + file)] + models
                    modelnames = ['Ideal'] + modelnames
                    first_noiseless = False
    palette = sns.color_palette('hls', len(models + hardware))
    for i, model in enumerate(models):
        plt.bar([i], model, color=palette[i], width=1, label=modelnames[i])
    for j, platform in enumerate(hardware):
        plt.bar(
            [j + len(models) + 1],
            platform,
            color=palette[j + len(models)],
            width=1,
            label=hardwarenames[j])
    plt.ylabel('P(success)')
    plt.xticks([4, 10.5], ['Models', 'Hardware'])
    plt.gca().xaxis.grid(False)
    plt.gca().yaxis.grid(True)
    plt.gca().set_axisbelow(True)
    plt.gca().xaxis.set_ticks_position('none')
    plt.gca().yaxis.set_ticks_position('none')
    plt.setp(plt.gca().spines.values(), color='#B5B5B5')
    leg = plt.legend(bbox_to_anchor=(1.05, 0.86), loc=2, borderaxespad=0.)
    plt.savefig(
        'figures/psuccess.pdf',
        bbox_extra_artists=(leg, ),
        bbox_inches='tight')
    return plt


def get_psuccess(filename):
    '''Compute the success probability of the HHL protocol from the statistics
    stored in the file.

    :param filename: (str) Name of the file with the statistics.
    :return: (float) The success probability.
    '''
    with open(filename, 'r') as file:
        count_dict = literal_eval(file.read())
    try:
        succ_rotation_fail_swap = count_dict['11']
    except KeyError:
        succ_rotation_fail_swap = 0
    try:
        succ_rotation_succ_swap = count_dict['01']
    except KeyError:
        succ_rotation_succ_swap = 0
    succ_rotation = succ_rotation_succ_swap + succ_rotation_fail_swap
    try:
        prob_swap_test_success = succ_rotation_succ_swap / succ_rotation
    except ZeroDivisionError:
        prob_swap_test_success = 0
    return prob_swap_test_success
